import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../data/_data.dart';
import '../../ui_kit/_ui_kit.dart';
import 'package:sun_stickers/states/shared_redux/_shared_redux.dart';
import 'package:sun_stickers/ui/_ui.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class StickerListView extends StatefulWidget {
  const StickerListView({super.key, this.isReversed = false, this.sortedStickers = true});
  
  final bool isReversed;
  final bool sortedStickers;
  
  @override
  State<StickerListView> createState() => StickerListViewState();
}

class StickerListViewState extends State<StickerListView> {
  bool get isReversed => widget.isReversed;
  bool get sortedStickers => widget.sortedStickers;

  @override
  Widget build(BuildContext context) {
    final isDark = Theme.of(context).brightness == Brightness.dark;
    return StoreConnector<SharedState, List<Sticker>>(
      converter: (Store<SharedState> store) {
        return sortedStickers ? StoreProvider.of<SharedState>(context).state.stickersByCategory : StoreProvider.of<SharedState>(context).state.stickers;
      },
      builder: (context, stickers) {
        return SizedBox(
          height: 200,
          child: ListView.separated(
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsets.only(top: 20),
              itemBuilder: (_, index) {
                Sticker sticker = isReversed ? stickers.reversed.toList()[index] : stickers[index];
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (_) => StickerDetailScreen(stickerId: sticker.id,)));
                  },
                  child: Container(
                    width: 160,
                    decoration: BoxDecoration(
                      color: isDark ? AppColor.dark : Colors.white,
                      //color: Colors.red,
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.asset(sticker.image, scale: 6),
                          Text(
                            "\$${sticker.price}",
                            style: AppTextStyle.h3Style.copyWith(color: AppColor.accent),
                          ),
                          Text(
                            sticker.name,
                            style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (_, __) {
                return Container(
                  width: 50,
                );
              },
              itemCount: stickers.length
          ),
        );
      },
    );
  }
}

