import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sun_stickers/ui/_ui.dart';

import '../../data/_data.dart';
import '../../ui_kit/_ui_kit.dart';
import '../widgets/_widgets.dart';

import 'package:flutter/material.dart' hide Badge;
import 'package:sun_stickers/states/shared_redux/_shared_redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class StickerDetailScreen extends StatefulWidget {
  const StickerDetailScreen({super.key, required this.stickerId});
  final int stickerId;

  @override
  State<StickerDetailScreen> createState() => StickerDetailScreenState();
}

class StickerDetailScreenState extends State<StickerDetailScreen> {
  int get stickerId => widget.stickerId;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<SharedState, Sticker>(
        converter: (Store<SharedState> store) {
          return StoreProvider.of<SharedState>(context).state.stickers.firstWhere((element) => element.id == stickerId);
        },
        builder: (context, sticker) {
          return Scaffold(
            appBar: _appBar(context),
            body: Center(child: Image.asset(sticker.image, scale: 2)),
            floatingActionButton: _floatingActionButton(),
            floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
            bottomNavigationBar: _bottomAppBar(context),
          );
        }
    );
  }

  PreferredSizeWidget _appBar(BuildContext context) {
    return AppBar(
      leading: IconButton(
        onPressed: () => Navigator.of(context).pop(),
        icon: const Icon(Icons.arrow_back),
      ),
      title: Text(
        'Sticker Detail Screen',
        style: TextStyle(color: Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white),
      ),
      actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert))],
    );
  }

  Widget _floatingActionButton() {
    return StoreConnector<SharedState, bool>(
        converter: (Store<SharedState> store) {
          return StoreProvider.of<SharedState>(context).state.stickers.firstWhere((element) => element.id == stickerId).favorite;
        },
        builder: (context, favorite) {
          return FloatingActionButton(
            elevation: 0.0,
            backgroundColor: AppColor.accent,
            onPressed: () {
              StoreProvider.of<SharedState>(context).dispatch(AddRemoveFavoriteAction(stickerId: stickerId));
            },
            child: favorite ? const Icon(AppIcon.heart) : const Icon(AppIcon.outlinedHeart),
          );
        }
    );
  }

  Widget _bottomAppBar(BuildContext context) {
    return ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        child: StoreConnector<SharedState, Sticker> (
          converter: (Store<SharedState> store) => store.state.stickers.firstWhere((element) => element.id == stickerId),
          builder: (context, sticker) {
            return BottomAppBar(
                child: SizedBox(
                    height: 300,
                    child: Container(
                      color: Theme.of(context).brightness == Brightness.dark ? AppColor.dark : Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(30),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  RatingBar.builder(
                                    itemPadding: EdgeInsets.zero,
                                    itemSize: 20,
                                    initialRating: sticker.score,
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    glow: false,
                                    ignoreGestures: true,
                                    itemBuilder: (_, __) => const FaIcon(
                                      FontAwesomeIcons.solidStar,
                                      color: AppColor.yellow,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print('$rating');
                                    },
                                  ),
                                  const SizedBox(width: 15),
                                  Text(
                                    sticker.score.toString(),
                                    style: Theme.of(context).textTheme.titleMedium,
                                  ),
                                  const SizedBox(width: 5),
                                  Text(
                                    "(${sticker.voter})",
                                    style: Theme.of(context).textTheme.titleMedium,
                                  )
                                ],
                              ),
                              const SizedBox(height: 15),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "\$${sticker.price}",
                                    style: Theme.of(context).textTheme.displayLarge?.copyWith(color: AppColor.accent),
                                  ),
                                  CounterButton(
                                    onIncrementTap: () {
                                      StoreProvider.of<SharedState>(context).dispatch(IncreaseQuantityAction(stickerId: sticker.id));
                                    },
                                    onDecrementTap: () {
                                      StoreProvider.of<SharedState>(context).dispatch(DecreaseQuantityAction(stickerId: sticker.id));
                                    },
                                    label: Text(
                                      sticker.quantity.toString(),
                                      style: Theme.of(context).textTheme.displayLarge,
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(height: 15),
                              Text(
                                "Description",
                                style: Theme.of(context).textTheme.displayMedium,
                              ),
                              const SizedBox(height: 15),
                              Text(
                                sticker.description,
                                style: Theme.of(context).textTheme.titleMedium,
                              ),
                              const SizedBox(height: 30),
                              SizedBox(
                                width: double.infinity,
                                height: 45,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 30),
                                  child: ElevatedButton(
                                    onPressed: () {
                                      StoreProvider.of<SharedState>(context).dispatch(AddToCartAction(stickerId: sticker.id));
                                    },
                                    child: const Text("Add to cart"),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                )
            );
          },
        )
    );
  }
}


