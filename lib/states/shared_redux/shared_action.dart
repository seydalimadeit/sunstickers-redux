import '../../data/_data.dart';

class CategoryTapAction {
  CategoryTapAction({ required this.category });
  final StickerCategory category;
}

class ToggleThemeAction {
  ToggleThemeAction();
}

class IncreaseQuantityAction {
  IncreaseQuantityAction({ required this.stickerId });
  final int stickerId;
}

class DecreaseQuantityAction {
  DecreaseQuantityAction({ required this.stickerId });
  final int stickerId;
}

class AddToCartAction {
  AddToCartAction({ required this.stickerId });
  final int stickerId;
}

class AddRemoveFavoriteAction {
  AddRemoveFavoriteAction({ required this.stickerId });
  final int stickerId;
}

class CheckOutTap {
  CheckOutTap();
}