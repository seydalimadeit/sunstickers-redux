import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';

import '../../data/_data.dart';
import 'shared_action.dart';
import 'shared_state.dart';

/// Reducer
final sharedReducer = combineReducers<SharedState>([
  TypedReducer<SharedState, CategoryTapAction>(_onCategoryTap),
  TypedReducer<SharedState, ToggleThemeAction>(_onToggleThemeTap),
  TypedReducer<SharedState, IncreaseQuantityAction>(_onIncreaseQuantityTap),
  TypedReducer<SharedState, DecreaseQuantityAction>(_onDecreaseQuantityTap),
  TypedReducer<SharedState, AddToCartAction>(_onAddToCartTap),
  TypedReducer<SharedState, AddRemoveFavoriteAction>(_onAddRemoveFavoriteTap),
  TypedReducer<SharedState, CheckOutTap>(_onCheckOutTap),
]);

SharedState _onCategoryTap(SharedState state, CategoryTapAction action) {
  final categories = state.categories.map((e) {
    if (e.type == action.category.type) {
      return e.copyWith(isSelected: true);
    } else {
      return e.copyWith(isSelected: false);
    }
  }).toList();

  if (action.category.type == StickerType.all) {
    final stickersByCategory = state.stickers;
    return state.copyWith(categories: categories,stickersByCategory: stickersByCategory);
  } else {
    final stickersByCategory = state.stickers.where((e) => e.type == action.category.type).toList();
    return state.copyWith(categories: categories,stickersByCategory: stickersByCategory);
  }
}

SharedState _onToggleThemeTap(SharedState state, ToggleThemeAction action) {
  final isLight = state.light ? false : true;
  return state.copyWith(light: isLight);
}

SharedState _onIncreaseQuantityTap(SharedState state, IncreaseQuantityAction action) {
  final stickers = state.stickers.map((e) {
    if (e.id == action.stickerId) {
      return e.copyWith(quantity: e.quantity + 1);
    } else {
      return e;
    }
  }).toList();
  return state.copyWith(stickers: stickers);
}

SharedState _onDecreaseQuantityTap(SharedState state, DecreaseQuantityAction action) {
  final stickers = state.stickers.map((e) {
    if (e.id == action.stickerId) {
      return e.quantity == 1 ? e : e.copyWith(quantity: e.quantity - 1);
    } else {
      return e;
    }
  }).toList();
  return state.copyWith(stickers: stickers);
}

SharedState _onAddToCartTap(SharedState state, AddToCartAction action) {
  final stickers = state.stickers.map((e) {
    if (e.id == action.stickerId) {
      return e.copyWith(cart: true);
    } else {
      return e;
    }
  }).toList();

  return state.copyWith(stickers: stickers, cart: stickers.where((e) => e.cart).toList());
}

SharedState _onAddRemoveFavoriteTap(SharedState state, AddRemoveFavoriteAction action) {
  final stickers = state.stickers.map((e) {
    if (e.id == action.stickerId) {
      return e.copyWith(favorite: !e.favorite);
    } else {
      return e;
    }
  }).toList();
  return state.copyWith(stickers: stickers, favorites: stickers.where((e) => e.favorite).toList());
}

SharedState _onCheckOutTap(SharedState state, CheckOutTap action) {
  Set<int> cartIds = <int>{};
  for (var item in state.cart) {
    cartIds.add(item.id);
  }
  final stickers = state.stickers.map((e) {
    if (cartIds.contains(e.id)) {
      return e.copyWith(cart: false, quantity: 1);
    } else {
      return e;
    }
  }).toList();
  return state.copyWith(cart: stickers.where((e) => e.cart).toList());
}