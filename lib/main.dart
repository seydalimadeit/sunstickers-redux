import 'package:flutter/material.dart';

import 'package:sun_stickers/states/shared_redux/_shared_redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';


import 'ui/_ui.dart';
import 'ui_kit/_ui_kit.dart';

void main() {
  final store = Store<SharedState>(
      sharedReducer,
      initialState: SharedState.initial()
  );
  runApp(MyApp(store: store));
}

class MyApp extends StatelessWidget {
  final Store<SharedState> store;
  const MyApp({super.key, required this.store});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StoreProvider<SharedState>(
      store: store,
      child: const StartApp(),
    );
  }
}

class StartApp extends StatelessWidget {
  const StartApp({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<SharedState, bool>(
      converter: (Store<SharedState> store) {
        return StoreProvider.of<SharedState>(context).state.light;
      },
      builder: (context, state) {
        return MaterialApp(
          title: 'Sunny Stickers',
          theme: state ? AppTheme.lightTheme : AppTheme.darkTheme,
          home: const HomeScreen(),
        );
      }
    );
  }
}
